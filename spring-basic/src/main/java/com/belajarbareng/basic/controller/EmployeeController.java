package com.belajarbareng.basic.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.belajarbareng.basic.repository.EmployeeRepository;
import com.belajarbareng.basic.util.Response;

import com.belajarbareng.basic.entity.Employee;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;
    
    @GetMapping("")
    public ResponseEntity<List<Employee>> getEmployees() {
        List<Employee> employees = employeeRepository.findAll();

        return ResponseEntity.ok().body(employees);
    }

    @PostMapping("")
    public ResponseEntity<Response> setEmployee(@RequestBody Employee employee) {
        Employee emp = employeeRepository.save(employee);
        if (emp == null) {
            return new ResponseEntity(new Response<>("Save employee failed"), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(new Response<>("Save employee successfully", emp), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> getEmployee(@PathVariable Long id) {
        Optional<Employee> employee = employeeRepository.findById(id);
        if (!employee.isPresent()) {
            return new ResponseEntity(new Response<>("Employee not found", employee), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(new Response<>("Employee found", employee), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Response> updateEmployee(@RequestBody Employee employee, @PathVariable Long id) {
        if (employeeRepository.existsById(id)) {
            return new ResponseEntity(new Response<>("Employee ID not found"), HttpStatus.BAD_REQUEST);
        }

        employee.setId(id);
        Employee emp = employeeRepository.save(employee);

        return new ResponseEntity(new Response<>("Employee updated", emp), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteEmployee(@PathVariable Long id) {
        employeeRepository.deleteById(id);

        return new ResponseEntity(new Response<>("Employee deleted"), HttpStatus.OK);
    }
}
