package com.belajarbareng.basic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.belajarbareng.basic.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    
}
